# SMT Tape Holder Generator

OpenSCAD code to parametrically generate SMT tape holders for hand assembly of circuit boards.

![](holder01.jpg)

## Description and Instructions

This [OpenSCAD](http://www.openscad.org/index.html) code
parametrically generates a 3D-printable holder for surface mount tape.
It is intended to generate simple assembly aids for hand assembly of
circuit boards with surface-mounted components. You can make simple
modifications to constants definied at the beginning of the program to
generate holders with any desired number of channels, for aribtrary
tape widths.

The generated holder is intended for use with cut tape feeding from
left to right, with the sprocket holes at the top. It does not include
a reel holder. It has a "buckle" at the left edge to hold the folded
over cover tape.

Import the SMT_Tape_Holder.scad file with the "use" statement, and
then call the holder() function to generate the holder model. If you
run the SMT_Tape_Holder.scad file directly, it will generate an
example holder.

Once everything looks right in OpenSCAD, render the design, export an
STL file, import that file into your slicer, and generate code for
your 3D printer. You may need to include removable support structures
in your print to support the bridged rails which hold down the tapes.

If you need more flexibility, try varying the constants defined near
the beginning of SMT_Tape_Holder.scad.

See the other *.scad files for example holder designs, and the *.stl
files for their rendered outputs.

## Copying

This project is released under GPLv3. See file *COPYING.md* for the
full license terms.
